package com.priceapi.test.demo;

import com.priceapi.test.demo.Application.Data.Requests.GetProductPriceRequest;
import com.priceapi.test.demo.Application.Data.Responses.GetProductPriceResponse;
import com.priceapi.test.demo.Application.Data.Responses.ResponseWrapper;
import com.priceapi.test.demo.Application.UseCases.GetProductPriceUseCase;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class DemoApplicationTests {

    @Autowired
    GetProductPriceUseCase getProductPriceUseCase;

    @Test
    void contextLoads() {
        assertThat(getProductPriceUseCase).isNotNull();
    }
    @Test
    void testPrices(){
        Float price_test1 = getPrice("2020-06-14 10:00", 35455, 1);
        Float price_test2 = getPrice("2020-06-14 16:00", 35455, 1);
        Float price_test3 = getPrice("2020-06-14 21:00", 35455, 1);
        Float price_test4 = getPrice("2020-06-15 10:00", 35455, 1);
        Float price_test5 = getPrice("2020-06-16 21:00", 35455, 1);

        assertThat(price_test1).isNotNull().isEqualTo(35.5f);
        assertThat(price_test2).isNotNull().isEqualTo(25.45f);
        assertThat(price_test3).isNotNull().isEqualTo(35.5f);
        assertThat(price_test4).isNotNull().isEqualTo(30.5f);
        assertThat(price_test5).isNotNull().isEqualTo(38.95f);
    }
    Float getPrice(String date, int productId, int brandId) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        try {
            Date d = sdf.parse(date);
            ResponseWrapper<GetProductPriceResponse> rspW = getProductPriceUseCase.getProductPrice(new GetProductPriceRequest(
                    d, productId, brandId
            ));
            if (rspW.hasResult()) {
                return rspW.getResult().getPrice().getPrice();
            } else {
                return null;
            }
        }catch (ParseException e){
            e.printStackTrace();
            return null;
        }
    }

}
