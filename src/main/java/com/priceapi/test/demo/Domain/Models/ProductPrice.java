package com.priceapi.test.demo.Domain.Models;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.priceapi.test.demo.Domain.Models.Keys.ProductPriceKey;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import java.io.Serializable;
import java.util.Date;

@Entity
@IdClass(ProductPriceKey.class)
public class ProductPrice implements Serializable {
    @Id
    private int brandId;
    @Id
    private int priceList;
    @Id
    private int productId;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date startDate;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endDate;
    private int priority;
    private float price;
    private String currency;

    public ProductPrice(){

    }
    public ProductPrice(
            final int brandId,
            final Date startDate,
            final Date endDate,
            final int priceList,
            final int productId,
            final int priority,
            final float price,
            final String currency
    ) {
        this.brandId = brandId;
        this.startDate = startDate;
        this.endDate = endDate;
        this.priceList = priceList;
        this.productId = productId;
        this.priority = priority;
        this.price = price;
        this.currency = currency;
    }

    public int getBrandId() {
        return brandId;
    }

    public Date getStartDate() {
        return startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public int getPriceList() {
        return priceList;
    }

    public int getProductId() {
        return productId;
    }
    @JsonIgnore
    public int getPriority() {
        return priority;
    }

    public float getPrice() {
        return price;
    }

    public String getCurrency() {
        return currency;
    }
}
