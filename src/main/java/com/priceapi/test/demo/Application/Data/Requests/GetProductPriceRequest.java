package com.priceapi.test.demo.Application.Data.Requests;

import java.util.Date;

public class GetProductPriceRequest {
    private Date date;
    private int productId;
    private int brandId;

    public GetProductPriceRequest(final Date date, final int productId, final int brandId) {
        this.date = date;
        this.productId = productId;
        this.brandId = brandId;
    }

    public Date getDate() {
        return date;
    }

    public int getProductId() {
        return productId;
    }

    public int getBrandId() {
        return brandId;
    }
}
