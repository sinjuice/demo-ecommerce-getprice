package com.priceapi.test.demo.Domain.Models.Keys;

import java.io.Serializable;
import java.util.Objects;

public class ProductPriceKey implements Serializable {
    private int brandId;
    private int productId;
    private int priceList;
    public ProductPriceKey() {

    }
    public ProductPriceKey(final int brandId, final int priceList, final int productId){
        this.brandId = brandId;
        this.productId = productId;
        this.priceList = priceList;
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProductPriceKey priceKey = (ProductPriceKey) o;
        return brandId == priceKey.brandId &&
                priceList == priceKey.priceList &&
                productId == priceKey.productId;
    }

    @Override
    public int hashCode() {
        return Objects.hash(brandId, priceList, productId);
    }
}
