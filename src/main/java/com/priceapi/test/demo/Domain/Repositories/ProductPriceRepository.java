package com.priceapi.test.demo.Domain.Repositories;

import com.priceapi.test.demo.Domain.Models.ProductPrice;

import java.util.Date;
import java.util.Optional;

public interface ProductPriceRepository {
    Optional<ProductPrice> getApplicableProductPrice(final Date applyDate, final int productId, final int brandId);
}
