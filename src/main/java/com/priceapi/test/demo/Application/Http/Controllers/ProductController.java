package com.priceapi.test.demo.Application.Http.Controllers;


import com.priceapi.test.demo.Application.Data.Requests.GetProductPriceRequest;
import com.priceapi.test.demo.Application.Data.Responses.GetProductPriceResponse;
import com.priceapi.test.demo.Application.Data.Responses.ResponseWrapper;
import com.priceapi.test.demo.Application.UseCases.GetProductPriceUseCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

@RestController
@RequestMapping(produces = "application/json", path = "v1/")
public class ProductController {

    @Autowired
    GetProductPriceUseCase getProductPriceUseCase;


    @RequestMapping(value = "/brand/{brandId}/product/{productId}/price", method = RequestMethod.GET)
    public ResponseEntity<ResponseWrapper<GetProductPriceResponse>> getProductPrice(
            @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss") Date applyDate,
            @PathVariable int brandId,
            @PathVariable int productId

    ) {
        GetProductPriceRequest request = new GetProductPriceRequest(applyDate,productId, brandId);
        ResponseWrapper<GetProductPriceResponse> rsp = getProductPriceUseCase.getProductPrice(request);
        return ResponseEntity.status(rsp.getResultCode()).body(rsp);
    }
}
