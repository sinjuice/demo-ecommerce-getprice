package com.priceapi.test.demo.Application.Data.Responses;

import com.priceapi.test.demo.Domain.Models.ProductPrice;

public class GetProductPriceResponse {
    private ProductPrice price;
    public GetProductPriceResponse(final ProductPrice result) {
        this.price = result;
    }
    public ProductPrice getPrice() {
        return price;
    }
}
