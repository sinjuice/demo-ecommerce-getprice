package com.priceapi.test.demo.Infrastructure.Repositories;

import com.priceapi.test.demo.Domain.Models.ProductPrice;
import com.priceapi.test.demo.Domain.Repositories.ProductPriceRepository;

import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Optional;

@Repository
public class H2ProductPriceRepository implements ProductPriceRepository {

    @PersistenceContext
    EntityManager manager;

    public Optional<ProductPrice> getApplicableProductPrice(final Date applyDate, final int productId, final int brandId) {
        Timestamp applyTimestamp = new Timestamp(applyDate.getTime());
        TypedQuery<ProductPrice> query = manager.createQuery("SELECT p FROM ProductPrice p " +
                                "WHERE p.productId = ?1 and p.brandId = ?2  and ?3 between p.startDate and p.endDate " +
                                "order by p.priority desc", ProductPrice.class);
        query.setParameter(1, productId)
         .setParameter(2, brandId)
         .setParameter(3, applyTimestamp);
        query.setMaxResults(1);
        try {
            ProductPrice result = query.getSingleResult();
            return Optional.of(result);
        }
        catch (NoResultException ex){
            return Optional.empty();
        }
    }
}
