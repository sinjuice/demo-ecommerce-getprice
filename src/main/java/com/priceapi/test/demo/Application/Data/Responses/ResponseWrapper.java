package com.priceapi.test.demo.Application.Data.Responses;


public class ResponseWrapper<T> {

    private int resultCode;
    private String resultMessage;
    private T resultObject;
    public ResponseWrapper(final int resultCode, final String resultMessage, final T resultObject){
        this.resultCode = resultCode;
        this.resultMessage = resultMessage;
        this.resultObject = resultObject;
    }
    public ResponseWrapper(final T resultObject){
        this(200, "OK", resultObject);
    }
    public boolean hasResult() {
        return resultObject != null;
    }
    public int getResultCode(){
        return resultCode;
    }
    public String getResultMessage(){
        return resultMessage;
    }
    public T getResult() {
        return resultObject;
    }

    public static <E> ResponseWrapper<E> notFound() {
        return new ResponseWrapper<E>(404, "NOT_FOUND", null);
    }
    public static <E> ResponseWrapper<E> error(final String message){
        return new ResponseWrapper<E>(500, message, null);
    }
}
