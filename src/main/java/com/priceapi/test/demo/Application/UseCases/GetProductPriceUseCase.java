package com.priceapi.test.demo.Application.UseCases;

import com.priceapi.test.demo.Application.Data.Requests.GetProductPriceRequest;
import com.priceapi.test.demo.Application.Data.Responses.GetProductPriceResponse;
import com.priceapi.test.demo.Application.Data.Responses.ResponseWrapper;
import com.priceapi.test.demo.Domain.Models.ProductPrice;
import com.priceapi.test.demo.Infrastructure.Repositories.H2ProductPriceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class GetProductPriceUseCase {

    @Autowired
    H2ProductPriceRepository repository;

    public ResponseWrapper<GetProductPriceResponse> getProductPrice(final GetProductPriceRequest request) {
        try {
            Optional<ProductPrice> price = repository.getApplicableProductPrice(request.getDate(), request.getProductId(), request.getBrandId());
            if(price.isEmpty()){
                return ResponseWrapper.notFound();
            } else{
                return new ResponseWrapper<>(new GetProductPriceResponse(price.get()));
            }
        } catch (Exception e){
            return ResponseWrapper.error(e.getMessage());
        }
    }
}
